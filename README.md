Signewise REST API (trial task) Manual
=========================================

Summary
-------
The project is Signwise trial task solution. It simple implements REST JSON API

Project info
-------------------------
* Maven
* No-xml Spring MVC 4 web application for Servlet 3.0 environment
* JUnit/Mockito

Installation
------------

First you have to clone the repository to your local machine, use the following commands:

```bash
    git clone https://ilves@bitbucket.org/ilves/signwisetest.git
    cd signwisetest
```

Run the tests
-------------

```bash
	mvn test
```

Run the project
----------------

With tests:

```bash
	mvn test tomcat7:run
```

Without tests:

```bash
	mvn tomcat7:run
```

Test on the browser / SoapUI
----------------------------

You can use the API on the following url:
	http://localhost:8080/signwise/

However only POST requests are supported. The endpoints of REST requests are:

* http://localhost:8080/signwise/getInfo
* http://localhost:8080/signwise/getChain
* http://localhost:8080/signwise/validateCertificate
* http://localhost:8080/signwise/validateSignedMessage

Generate Javadoc
----------------------------
```bash
	mvn javadoc:javadoc
```