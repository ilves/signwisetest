package ee.golive.api.response;

/**
 *  This class models API response containing certificate information
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class CertificateInfoResponse extends ApiResponse {

    /**
     * Certificate subject field
     */
    private String subject;

    /**
     * Certificate issuer field
     */
    private String issuer;

    /**
     * Certificate validity start date
     */
    private String notBefore;

    /**
     * Certificate expire date
     */
    private String notAfter;

    /**
     * OCSP service url
     */
    private String ocspUrl;

    /**
     * Certificate key usages
     */
    private String keyUsages;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(String notBefore) {
        this.notBefore = notBefore;
    }

    public String getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(String notAfter) {
        this.notAfter = notAfter;
    }

    public String getOcspUrl() {
        return ocspUrl;
    }

    public void setOcspUrl(String ocspUrl) {
        this.ocspUrl = ocspUrl;
    }

    public String getKeyUsages() {
        return keyUsages;
    }

    public void setKeyUsages(String keyUsages) {
        this.keyUsages = keyUsages;
    }
}