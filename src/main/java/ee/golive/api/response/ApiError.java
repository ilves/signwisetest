package ee.golive.api.response;

/**
 *  This class models API error response
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ApiError extends ApiResponse {

    /**
     * Error message
     */
    private String message;

    /**
     * Error code
     */
    private int code;

    /**
     * Error occurrence timestamp
     */
    private long timestamp;

    /**
     * Error more detailed description
     */
    private String description;

    /**
     * Error stack trace
     */
    private String stacktrace;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }
}