package ee.golive.api.response;

/**
 *  This class models API response that includes input certificate chain
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class CertificateChainResponse extends ApiResponse {

    /**
     * Certificate chain string array where every instance is base64 encoded
     * certificate data.
     */
    private String[] chain;

    public String[] getChain() {
        return chain;
    }

    public void setChain(String[] chain) {
        this.chain = chain;
    }
}
