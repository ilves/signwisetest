package ee.golive.api.response;

/**
 * This class models API response containing certificate status
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class CertificateStatusResponse extends ApiResponse {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
