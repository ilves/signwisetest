package ee.golive.api.request;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *  This class models API requests including certificate base64 encoded body, signedHash and hash
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ValidateSignedMessageRequest {

    /**
     * Certificate base64 encoded string
     */
    @NotEmpty
    private String certificate;

    /**
     * String that is signed with the certificate
     */
    @NotEmpty
    private String signedHash;

    /**
     * Original string that was signed with the certificate
     * and produced the signed hash.
     */
    @NotEmpty
    private String hash;

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getSignedHash() {
        return signedHash;
    }

    public void setSignedHash(String signedHash) {
        this.signedHash = signedHash;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
