package ee.golive.api.exception;

/**
 *  This class represents the ApiException object that is used
 *  for API specific exceptions. It also contains explicitly handled
 *  exception codes.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ApiException extends Exception {

    /**
     * Input request JSON values validation fails, specified by the expected
     * model
     */
    public static final int ERROR_VALIDATION = 1;

    /**
     * Request input is not readable. More specifically the request input
     * is not valid JSON.
     */
    public static final int ERROR_INVALID_JSON = 2;

    /**
     * Main method, calls parent method with same signature
     *
     * @param message Exception message
     */
    public ApiException(String message) {
        super(message);
    }
}
