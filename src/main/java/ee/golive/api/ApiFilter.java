package ee.golive.api;

import ee.golive.helper.RequestWrapper;
import ee.golive.helper.ResponseWrapper;
import org.jboss.logging.Logger;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**
 *  This class contains API filter that is used to log the request body and
 *  response.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ApiFilter extends GenericFilterBean {

    /**
     * Logger object
     */
    Logger logger = Logger.getLogger(getClass());

    /**
     * Original API servlet request object
     */
    HttpServletRequest httpServletRequest;

    /**
     * Original API servlet response object
     */
    HttpServletResponse httpServletResponse;

    /**
     * Filter method that is called by the dispatcher servlet. Here we add custom request
     * and response wrappers, because input and output reader/writer can be accessed only
     * once.
     *
     * Here we also call logger methods to log the request input body and response to the
     * api request.
     *
     * @param servletRequest request
     * @param servletResponse response
     * @param filterChain filter chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        httpServletRequest = (HttpServletRequest) servletRequest;
        httpServletResponse = (HttpServletResponse) servletResponse;

        // Initiates the custom wrappers
        RequestWrapper myRequestWrapper = new RequestWrapper(httpServletRequest);
        ResponseWrapper myResponseWrapper = new ResponseWrapper(httpServletResponse);

        // Log the request body
        logger.info("REQUEST: " + myRequestWrapper.getBody());

        // Continue with the filter chain using our custom wrappers
        filterChain.doFilter(myRequestWrapper, myResponseWrapper);

        // Get the response
        String responseData = myResponseWrapper.getResponseData();
        writeOutputStream(responseData);

        // Log the request response
        logger.info("RESPONSE: " + responseData);
    }

    /**
     * Writes the response into original servlet response objects output stream
     *
     * @param response output to write
     * @throws IOException
     */
    private void writeOutputStream(String response) throws IOException {
        OutputStream outputStream = httpServletResponse.getOutputStream();
        outputStream.write(response.getBytes());
        outputStream.flush();
        outputStream.close();
    }
}