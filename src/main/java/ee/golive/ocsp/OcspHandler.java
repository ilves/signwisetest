package ee.golive.ocsp;

import ee.golive.helper.CertificateHelper;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.ocsp.*;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Security;
import java.security.cert.X509Certificate;

/**
 *  This class handles ocsp requests
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@Component
public class OcspHandler {

    /**
     * Certificate is OK
     */
    static final int STATUS_OK = 1;

    /**
     * Certificate is revoked
     */
    static final int STATUS_REVOKED = 2;

    /**
     * Certificate status is unknown
     */
    static final int STATUS_UNKNOWN = 3;

    @Autowired
    CertificateHelper certificateHelper;

    Logger logger = Logger.getLogger(getClass());

    /**
     * Gets certificate status from OCSP url
     *
     * @param certificate Source certificate
     * @return Certificate status
     * @throws Exception throws exception when the status can not be determined
     */
    public int getStatus(X509Certificate certificate) throws Exception {
        URL url = new URL(certificateHelper.getOCSPUrl(certificate));
        HttpURLConnection connection = (constructConnection(url));
        handleOutput(connection, getInfo(certificate));

        SingleResp[] responses = handleContent((InputStream) connection.getContent());
        if (responses == null || responses.length != 1) {
            throw new OcspException("OCSP Response was empty");
        }

        SingleResp resp = responses[0];
        return getStatusFromResponse(resp);
    }

    /**
     * Gets required info for the ocsp request from the certificate
     *
     * @param certificate source certificate
     * @return information in the byte array
     * @throws Exception
     */
    private byte[] getInfo(X509Certificate certificate) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        OCSPReqGenerator ocspReqGenerator = new OCSPReqGenerator();
        CertificateID certId = new CertificateID(CertificateID.HASH_SHA1, certificate, certificate.getSerialNumber());
        ocspReqGenerator.addRequest(certId);
        OCSPReq ocspReq = ocspReqGenerator.generate();
        return ocspReq.getEncoded();
    }

    /**
     * Handles the output to the request ie. writes request information
     * to the ocsp server.
     *
     * @param connection opened connection to the ocsp server
     * @param request request object
     * @throws Exception
     */
    private void handleOutput(HttpURLConnection connection, byte[] request) throws Exception {
        OutputStream out = connection.getOutputStream();
        DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
        dataOut.write(request);
        dataOut.flush();
        dataOut.close();

    }

    /**
     * Constructs correct connection to the ocsp server. I.e sets the valid
     * headers, methods etc.
     *
     * @param url OCSP server url
     * @return connection object
     * @throws Exception
     */
    private HttpURLConnection constructConnection(URL url) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "application/ocsp-request");
        connection.setRequestProperty("Accept", "application/ocsp-response");
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        return connection;
    }

    /**
     * Handles ocsp server response
     *
     * @param input response input stream
     * @return response object if possible and if the request was empty the null
     * @throws Exception
     */
    private SingleResp[] handleContent(InputStream input) throws Exception {
        OCSPResp OCSPResponse = new OCSPResp(input);
        BasicOCSPResp basicResponse = (BasicOCSPResp) OCSPResponse.getResponseObject();
        return (basicResponse != null) ? basicResponse.getResponses() : null;
    }

    /**
     * Reads certificate status from the ocsp server response
     *
     * @param response server response object
     * @return status of the certificate
     */
    private int getStatusFromResponse(SingleResp response) {
        Object status = response.getCertStatus();
        if (status == CertificateStatus.GOOD) {
            return STATUS_OK;
        } else if (status instanceof org.bouncycastle.ocsp.RevokedStatus) {
            return STATUS_REVOKED;
        } else {
            return STATUS_UNKNOWN;
        }
    }

    /**
     * Transforms the int status into readable string
     *
     * @param status status int value
     * @return status string value
     */
    public String getStatusLabel(int status) {
        switch (status) {
            case STATUS_OK:
                return "GOOD";
            case STATUS_REVOKED:
                return "REVOKED";
            default:
                return "UNKNOWN";
        }
    }
}