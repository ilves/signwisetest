package ee.golive.ocsp;

/**
 *  This class represents the OcspException object that is used
 *  for OCSP request specific exceptions. It also contains explicitly handled
 *  exception codes.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class OcspException extends Exception {

    /**
     * Main method, calls parent method with same signature
     *
     * @param message Exception message
     */
    public OcspException(String message) {
        super(message);
    }
}
