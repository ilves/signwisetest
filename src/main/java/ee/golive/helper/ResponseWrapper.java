package ee.golive.helper;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *  This class is for servlet response wrapper to capture and log
 *  request output.
 *
 *  As the output writer can be accessed only once
 *  we need to provide own wrapper to capture the output.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ResponseWrapper extends HttpServletResponseWrapper {

    /**
     * Own output steam
     */
    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    /**
     * Own writer
     */
    private PrintWriter writer = new PrintWriter(outputStream);

    /**
     * Use parent constructor
     * @param response response object
     * @throws IOException
     */
    public ResponseWrapper(HttpServletResponse response) throws IOException {
        super(response);
    }

    /**
     * Returns servlet output stream that writes into our managed
     * output stream.
     *
     * @return Servlet output stream object
     * @throws IOException
     */
    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                outputStream.write(b);
            }

            @Override
            public void write(byte[] b) throws IOException {
                outputStream.write(b);
            }
        };
    }

    /**
     * Returns our own managed writer
     *
     * @return writer object
     * @throws IOException
     */
    @Override
    public PrintWriter getWriter() throws IOException {
        return writer;
    }

    /**
     * Flushes writer or output stream buffers
     *
     * @throws IOException
     */
    @Override
    public void flushBuffer() throws IOException {
        if (writer != null) {
            writer.flush();
        } else if (outputStream != null) {
            outputStream.flush();
        }
    }

    /**
     * Returns data from output stream as string
     *
     * @return output stream as string
     */
    public String getResponseData() {
        return outputStream.toString();
    }
}