package ee.golive.helper;

import java.security.cert.X509Certificate;

/**
 *  This class models certificate chain hierarchy
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class Chain {

    /**
     * Certificate
     */
    private X509Certificate certificate;

    /**
     * Certificates parent object
     */
    private Chain parent;

    /**
     * Certificates child object
     */
    private Chain child;

    public X509Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    public Chain getParent() {
        return parent;
    }

    public void setParent(Chain parent) {
        this.parent = parent;
    }

    public Chain getChild() {
        return child;
    }

    public void setChild(Chain child) {
        this.child = child;
    }

    /**
     * Builds certificate chain from leaf (current) certificate and array of possible certificates
     *
     * @param certificates list of possible ceritificates
     * @return root certificate
     */
    public Chain buildChain(X509Certificate[] certificates) {
        boolean isCompleted = false;
        Chain root = this;
        while (!isCompleted) {
            isCompleted = true;
            for (X509Certificate cert : certificates) {
                if (root.getCertificate().getIssuerX500Principal().equals(cert.getSubjectX500Principal()) &&
                        !cert.equals(root.getCertificate())) {
                    Chain parent = new Chain();
                    parent.setCertificate(cert);
                    parent.setChild(root);
                    root.setParent(parent);
                    root = parent;
                    isCompleted = false;
                    break;
                }
            }
        }
        return root;
    }
}