package ee.golive.helper;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.Security;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *  Helper class for certificate related methods.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@Component
public class CertificateHelper {

    /**
     * Trust store location
     */
    @Value("${truststore}")
    private String truststoreLocation;

    /**
     * Trust store password
     */
    @Value("${truststore.password}")
    private String truststorePassword;

    /**
     * Logger object
     */
    public Logger logger = Logger.getLogger(CertificateHelper.class);

    /**
     * Converts raw base64 encoded string into X509Certificate
     *
     * @param rawCertificate base64 encoded representation of the certificate
     * @return certificate object
     * @throws CertificateException
     */
    public X509Certificate generateCertificate(String rawCertificate) throws CertificateException {
        return (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(
                new ByteArrayInputStream(Base64.decode(rawCertificate.getBytes()))
        );
    }

    /**
     * Transforms keyUsages boolean array into more readable string list
     *
     * @param keyUsages where true represents active usage
     * @return list of active key usages represented by descriptive string
     */
    public List<String> getKeyUsages(boolean[] keyUsages) {
        List<String> usages = new ArrayList<>();
        String[] possibleUsages = {
                "DigitalSignature", "NonRepudation", "KeyEncipherment", "DataEncipherment",
                "KeyAgreement", "KeyCertSign", "CRLSign", "EncipherOnly", "DecipherOnly"
        };
        for(int i = 0; i < possibleUsages.length; i++) {
            if (keyUsages[i]) usages.add(possibleUsages[i]);
        }
        return usages;
    }

    /**
     * Gets OCSP Url from the certificate. As this procedure is not implemented
     * by the cryptography library we have to do it manually.
     *
     * @param certificate source certificate
     * @return ocsp url of the certificate
     */
    public String getOCSPUrl(X509Certificate certificate) {
        // Official extension value for the authority info
        byte[] value =  certificate.getExtensionValue("1.3.6.1.5.5.7.1.1");

        DEROctetString oct = null;
        try {
            oct = (DEROctetString) (new ASN1InputStream(  new ByteArrayInputStream(value)).readObject());
        } catch (IOException e) {
            logger.info("Authority info is not presented in the certificate");
            return null;
        }

        AuthorityInformationAccess authorityInformationAccess = null;
        try {
            authorityInformationAccess = new AuthorityInformationAccess((ASN1Sequence) new ASN1InputStream(oct.getOctets()).readObject());
        } catch (IOException e) {
            logger.info("Authority info can not be accessed");
            return null;
        }

        AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
        for (AccessDescription accessDescription : accessDescriptions) {
            GeneralName gn = accessDescription.getAccessLocation();
            DERIA5String str = DERIA5String.getInstance(gn.getDERObject());
            String ocspUrl = str.getString();
            logger.info("Found OSCP URL: " + ocspUrl);
            return ocspUrl;
        }

        return null;
    }

    /**
     * Builds certificate chain
     *
     * @param certificate leaf certificate
     * @return certificate chain starting with the root
     */
    public String[] buildChain(X509Certificate certificate) {
        Chain leaf = new Chain();
        leaf.setCertificate(certificate);
        X509Certificate[] certs = getCertsFromTrustStore(truststoreLocation, truststorePassword);
        return transformChainToStringArray(leaf.buildChain(certs));

    }

    /**
     * Gets certificates from trust store
     *
     * @param location trust store location
     * @param password turst store password
     * @return array of certificates or null if exception occurs
     */
    public X509Certificate[] getCertsFromTrustStore(String location, String password) {
        KeyStore keyStore = null;
        InputStream store = null;

        try {
            keyStore = KeyStore.getInstance("JKS");
            store = CertificateHelper.class.getClassLoader().getResourceAsStream(location);
            keyStore.load(store, password.toCharArray());

            X509Certificate[] certs = new X509Certificate[keyStore.size()];

            Enumeration<String> alias = keyStore.aliases();
            int i = 0;
            while (alias.hasMoreElements()) {
                certs[i] = (X509Certificate) keyStore.getCertificate(alias.nextElement());
                i++;
            }

            return certs;

        } catch (Exception ignored) {
            // Assume we have no problems with the keystore certificates
        }
        return null;
    }

    /**
     * Transforms certificate chain into string array of the certificates
     *
     * @param chain starting from root
     * @return array of the base64 encoded certificates
     */
    public String[] transformChainToStringArray(Chain chain) {
        List<String> certificates = new ArrayList<>();
        while(chain != null) {
            try {
                String str = new String(Base64.encode(chain.getCertificate().getEncoded()));
                certificates.add(str);
            } catch (Exception ignore) {

            }
            chain = chain.getChild();
        }
        return certificates.toArray(new String[certificates.size()]);
    }

    /**
     * Verifies signed text
     *
     * @param certificate Certificate the text was signed
     * @param text Original text
     * @param signature Signed text
     * @return true if the text was signed using this certificate otherwise false
     */
    public boolean verify(X509Certificate certificate, String text, String signature) {
        Security.addProvider(new BouncyCastleProvider());
        Signature verifier = null;
        try {
            verifier = Signature.getInstance("RSA");
            verifier.initVerify(certificate.getPublicKey());
            verifier.update(Base64.decode(text.getBytes()));
            return verifier.verify(Base64.decode(signature.getBytes()));
        } catch (Exception ignore) {

        }
        return false;
    }
}