package ee.golive.controller;

import ee.golive.api.exception.ApiException;
import ee.golive.api.response.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 *  This class handles all API requests related exceptions.
 *  Some exceptions are handled explicitly to add more descriptive
 *  information about the exception and appropriate error code.
 *
 *  Exception handling is done here to remove unnecessary clutter from
 *  the controller class and other parts of the code.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@ControllerAdvice
class APIExceptionHandler {

    /**
     * ApiError object that will be automatically concerted into json object
     */
    private final ApiError error = new ApiError();

    /**
     * This method handles errors mainly related to JSON input parsing
     *
     * @param exception Request input processing related exception
     * @return Descriptive error object
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError requestInputValidationError(HttpMessageNotReadableException exception) {
        addCommonValues(exception);
        error.setDescription("JSON validation failed");
        error.setCode(ApiException.ERROR_INVALID_JSON);
        return error;
    }

    /**
     * This method handles errors mainly related to JSON request.
     * For example cases when the request is missing required fields
     *
     * @param exception Request input validation related exception
     * @return Descriptive error object
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError jsonObjectValidationError(MethodArgumentNotValidException exception) {
        addCommonValues(exception);
        error.setDescription("Request inputs validation failed");
        error.setCode(ApiException.ERROR_VALIDATION);
        return error;
    }

    /**
     * This method handles all other errors that are not explicitly
     * handled
     *
     * @param exception Unhandled exception
     * @return Descriptive error object
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    ApiError handleException(Exception exception) {
        addCommonValues(exception);
        error.setDescription(null);
        return error;
    }

    /**
     * Adds common values to the ApiError object
     *
     * @param exception Source exception
     */
    private void addCommonValues(Exception exception) {
        error.setTimestamp(new Date().getTime());
        error.setMessage(exception.getMessage());
        error.setStacktrace(getStackTraceAsString(exception));
    }

    /**
     * Returns exception stack trace as a string
     *
     * @param exception Source exception object
     * @return Stack trace as string
     */
    private String getStackTraceAsString(Exception exception) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        return sw.toString();
    }
}