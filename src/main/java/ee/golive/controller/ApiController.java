package ee.golive.controller;

import ee.golive.api.exception.ApiException;
import ee.golive.api.request.CertificateRequest;
import ee.golive.api.request.ValidateSignedMessageRequest;
import ee.golive.api.response.ApiResponse;
import ee.golive.api.response.CertificateChainResponse;
import ee.golive.api.response.CertificateInfoResponse;
import ee.golive.api.response.CertificateStatusResponse;
import ee.golive.helper.CertificateHelper;
import ee.golive.ocsp.OcspHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.cert.X509Certificate;

/**
 *  This file contains API requests controller.
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@RestController
public class ApiController implements SignwiseInterface {

    /**
     * OCSP requests handler
     */
    @Autowired
    OcspHandler ocspHandler;

    /**
     * Certificate related helper methods
     */
    @Autowired
    CertificateHelper certificateHelper;

    /**
     * Implements getInfo API request that is used for getting
     * detailed info about the certificate.
     *
     * @param request input of the request
     * @return detailed info of the certificate
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "/getInfo", method = RequestMethod.POST)
    public ApiResponse getInfo(@Valid @RequestBody CertificateRequest request) throws Exception {
        CertificateInfoResponse response = new CertificateInfoResponse();
        X509Certificate certificate = certificateHelper.generateCertificate(request.getCertificate());
        response.setIssuer(certificate.getIssuerDN().toString());
        response.setNotAfter(certificate.getNotAfter().toString());
        response.setNotBefore(certificate.getNotBefore().toString());
        response.setSubject(certificate.getSubjectDN().toString());
        response.setKeyUsages(certificateHelper.getKeyUsages(certificate.getKeyUsage()).toString());
        response.setOcspUrl(certificateHelper.getOCSPUrl(certificate));
        return response;
    }

    /**
     * Implements getChain API request that is used for getting
     * certificate chain for the input certificate.
     *
     * @param request input of the request
     * @return certificate chain of the input certificate
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "/getChain", method = RequestMethod.POST)
    public CertificateChainResponse getChain(@Valid @RequestBody CertificateRequest request) throws Exception {
        CertificateChainResponse response = new CertificateChainResponse();
        X509Certificate certificate = certificateHelper.generateCertificate(request.getCertificate());
        response.setChain(certificateHelper.buildChain(certificate));
        return response;
    }

    /**
     * Implements validateCertificate API request that is used to validate the input
     * certificate
     *
     * @param request input of the request
     * @return status of the certificate
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "/validateCertificate", method = RequestMethod.POST)
    public CertificateStatusResponse validateCertificate(@Valid @RequestBody CertificateRequest request) throws Exception {
        CertificateStatusResponse response = new CertificateStatusResponse();
        X509Certificate certificate = certificateHelper.generateCertificate(request.getCertificate());
        int status = ocspHandler.getStatus(certificate);
        response.setStatus(ocspHandler.getStatusLabel(status));
        return response;
    }

    /**
     * Implements validateSignedMessage API request that is used for validating
     * that signed hash is signed by specific certificate and is valid.
     *
     * @param request input of the request
     * @return empty ok response if the signature is ok, otherwise exception is raised
     * @throws Exception
     */
    @Override
    @RequestMapping(value = "/validateSignedMessage", method = RequestMethod.POST)
    public String validateSignedMessage(@Valid @RequestBody ValidateSignedMessageRequest request) throws Exception {
        X509Certificate certificate = certificateHelper.generateCertificate(request.getCertificate());
        if (!certificateHelper.verify(certificate, request.getHash(), request.getSignedHash())) {
            throw new ApiException("Not a valid signature");
        }
        return null;
    }
}