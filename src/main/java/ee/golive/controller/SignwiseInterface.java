package ee.golive.controller;

import ee.golive.api.request.CertificateRequest;
import ee.golive.api.request.ValidateSignedMessageRequest;
import ee.golive.api.response.ApiResponse;
import ee.golive.api.response.CertificateChainResponse;
import ee.golive.api.response.CertificateStatusResponse;

/**
 *  This file contains API requests interface
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
interface SignwiseInterface {
    public ApiResponse getInfo(CertificateRequest request) throws Exception;
    public CertificateChainResponse getChain(CertificateRequest request) throws Exception;
    public CertificateStatusResponse validateCertificate(CertificateRequest request) throws Exception;
    public String validateSignedMessage(ValidateSignedMessageRequest request) throws Exception;
}
