package ee.golive.config;

import ee.golive.api.ApiFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletRegistration;

/**
 *  This file contains web application initializer that replaces the
 *  web.xml file configuration
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * Specifies the servlet mappings for the dispatcher
     *
     * @return String array of the mappings
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * Specifies application related configuration classes
     *
     * @return Object array of application configuration classes
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {ApplicationConfig.class};
    }

    /**
     * Specifies servlets related configuration classes
     *
     * @return Object array of servlets related configuration classes
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {WebMvcConfig.class};
    }

    /**
     * Specifies filters that are used by the dispatcher servlet.
     * Here we add filter to ensure UTF8 encoding and filter log request
     * bodies and responses.
     *
     * @return Filter array of the filter objects
     */
    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return new Filter[] {new ApiFilter(), characterEncodingFilter};
    }

    /**
     * Custom Spring related servlet settings
     *
     * @param registration ServletRegistration object
     */
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setInitParameter("defaultHtmlEscape", "true");
        registration.setInitParameter("spring.profiles.active", "default");
    }
}