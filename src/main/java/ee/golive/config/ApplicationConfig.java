package ee.golive.config;

import org.springframework.context.annotation.Configuration;

/**
 *  This file contains application related configuration
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@Configuration
class ApplicationConfig {

}