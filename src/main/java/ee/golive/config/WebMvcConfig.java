package ee.golive.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 *  This file contains servlets related configuration
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
@ComponentScan("ee.golive")
@Configuration
@PropertySource("classpath:app.properties")
@EnableWebMvc
class WebMvcConfig extends WebMvcConfigurationSupport {

    /**
     * Bean for using configuration (properties) file.
     *
     * @return
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
