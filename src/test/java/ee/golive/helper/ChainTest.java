package ee.golive.helper;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.security.cert.X509Certificate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ChainTest {

    @InjectMocks
    private CertificateHelper certificateHelper = new CertificateHelper();

    @Test
    public void buildChain() throws Exception {
        X509Certificate[] certificates = certificateHelper.getCertsFromTrustStore("SignWiseTest.jks", "changeit");
        assertThat(certificates.length, is(2));

        X509Certificate certificate = certificateHelper.generateCertificate(getFile("testCertificate"));
        Chain leaf = new Chain();
        leaf.setCertificate(certificate);

        Chain root = leaf.buildChain(certificates);

        assertThat(root.getCertificate(), equalTo(certificates[1]));
        assertThat(root.getChild().getCertificate(), equalTo(certificates[0]));
        assertThat(root.getChild().getChild().getCertificate(), equalTo(certificate));
    }

    /**
     * Helper to read test certificate data from the test resources folder
     *
     * @param fileName input file name
     * @return content from the file
     */
    private String getFile(String fileName){
        String result = "";
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}