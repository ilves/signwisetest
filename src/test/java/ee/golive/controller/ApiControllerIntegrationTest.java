package ee.golive.controller;

import ee.golive.api.exception.ApiException;
import ee.golive.api.request.CertificateRequest;
import ee.golive.api.request.ValidateSignedMessageRequest;
import ee.golive.config.WebAppConfigurationAware;
import ee.golive.helper.CertificateHelper;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.http.MediaType;

import java.io.IOException;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *  This class is used to model and run API integration tests
 *
 *  @author Taavi Ilves
 *  @since 1.0
 */
public class ApiControllerIntegrationTest extends WebAppConfigurationAware {

    @InjectMocks
    private CertificateHelper certificateHelper = new CertificateHelper();

    @Test
    public void requestValidCertificateInformation() throws Exception {
        CertificateRequest request = new CertificateRequest();
        request.setCertificate(getFile("testCertificate"));
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/getInfo")
                .content(mapper.writeValueAsBytes(request)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasKey("keyUsages")))
                .andExpect(jsonPath("$", hasKey("issuer")))
                .andExpect(jsonPath("$", hasKey("ocspUrl")))
                .andExpect(jsonPath("$", hasKey("notAfter")))
                .andExpect(jsonPath("$", hasKey("subject")))
                .andExpect(jsonPath("$", hasKey("notBefore")))
                .andExpect(jsonPath("$.ocspUrl", is("http://ocsp.test.signwise.me/")));
    }

    @Test
    public void requestValidCertificateChain() throws Exception {
        CertificateRequest request = new CertificateRequest();
        request.setCertificate(getFile("testCertificate"));
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/getChain")
                .content(mapper.writeValueAsBytes(request)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.chain", hasSize(3)));
    }

    @Test
    public void validateValidSignedMessage() throws Exception {
        ValidateSignedMessageRequest request = new ValidateSignedMessageRequest();
        request.setCertificate(getFile("testCertificate"));
        request.setHash("MCEwCQYFKw4DAhoFAAQUAQIDBAUGBwgJAAECAwQFBgcICQA=");
        request.setSignedHash("fIltUSyM/FGYLr2zvEpiFTGlZOp0L9g38QGRV7OCZWzY1" +
                "rAScJyupxJ0br9nks3znTygZ7JC+GZ2aaPPORS9zI1QX1LMPyub75vSDUSt" +
                "9mMNLmHAQRfbyUxYS0EoW3bm9VEyYzbv/R3pjQ9ghO3SuaJHlmUllpOPd32" +
                "98HT5VgPWscX4jJgqwulFe6nHRTo6d+81TwxLiSmo73JoCbw7o9b+Mqa6rh" +
                "InYkviM81TTWrZLWqk1vFDucmqi+gBjO4rZRulztxrhA+wXM5ELA68eJlWO" +
                "ro8Ptfi0uuasLUs9MCpbVUbCe9H/witcM6DnC8LwrdPr9C2FPuWaCttcQam9Q==");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/validateSignedMessage")
                .content(mapper.writeValueAsBytes(request)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void validateInvalidSignedMessage() throws Exception {
        ValidateSignedMessageRequest request = new ValidateSignedMessageRequest();
        request.setCertificate(getFile("testCertificate"));
        request.setHash("AQUAQIDBAUGBwgJAAECAwQFBgcICQA=");
        request.setSignedHash("fIltUSyM/FGYLr2zvEpiFTGlZOp0L9g38QGRV7OCZWzY1" +
                "rAScJyupxJ0br9nks3znTygZ7JC+GZ2aaPPORS9zI1QX1LMPyub75vSDUSt" +
                "9mMNLmHAQRfbyUxYS0EoW3bm9VEyYzbv/R3pjQ9ghO3SuaJHlmUllpOPd32" +
                "98HT5VgPWscX4jJgqwulFe6nHRTo6d+81TwxLiSmo73JoCbw7o9b+Mqa6rh" +
                "InYkviM81TTWrZLWqk1vFDucmqi+gBjO4rZRulztxrhA+wXM5ELA68eJlWO" +
                "ro8Ptfi0uuasLUs9MCpbVUbCe9H/witcM6DnC8LwrdPr9C2FPuWaCttcQam9Q==");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/validateSignedMessage")
                .content(mapper.writeValueAsBytes(request)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

    @Test
    public void jsonParseAndValidationErrors() throws Exception {
        mockMvc.perform(post("/getInfo")
                .content("{}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.code", is(ApiException.ERROR_VALIDATION)));

        mockMvc.perform(post("/getInfo")
                .content("{").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.code", is(ApiException.ERROR_INVALID_JSON)));
    }

    @Test
    public void validateCertificate() throws Exception {
        CertificateRequest request = new CertificateRequest();
        request.setCertificate(getFile("testCard"));
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/validateCertificate")
                .content(mapper.writeValueAsBytes(request)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("GOOD")));
    }

    /**
     * Helper to read test certificate data from the test resources folder
     *
     * @param fileName input file name
     * @return content from the file
     */
    private String getFile(String fileName){
        String result = "";
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}